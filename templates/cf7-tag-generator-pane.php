<?php

//var_dump($args);

?>
<div class="control-box">
    <fieldset>
        <legend>Make sure you have set the public key on the <a href="options-general.php?page=cim-encrypted-fields-options">settings page</a> or the field will not be encrypted.</legend>

        <table class="form-table">
            <tbody>
                <tr>
                    <th scope="row">
                        <?php echo esc_html( __( 'Field type', 'contact-form-7' ) ); ?>
                    </th>
                    <td>
                        <fieldset>
                            <legend class="screen-reader-text"><?php echo esc_html( __( 'Field type', 'contact-form-7' ) ); ?></legend>
                            <label><input type="checkbox" name="required" /> <?php echo esc_html( __( 'Required field', 'contact-form-7' ) ); ?></label>
                        </fieldset>
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="encrypted-name">
                            <?php echo esc_html( __( 'Name', 'contact-form-7' ) ); ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" name="name" class="tg-name oneline" id="encrypted-name" />
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="encrypted-id">
                            <?php echo esc_html( __( 'Id attribute', 'contact-form-7' ) ); ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" name="id" class="idvalue oneline option" id="encrypted-id" />
                    </td>
                </tr>

                <tr>
                    <th scope="row">
                        <label for="encrypted-class">
                            <?php echo esc_html( __( 'Class attribute', 'contact-form-7' ) ); ?>
                        </label>
                    </th>
                    <td>
                        <input type="text" name="class" class="classvalue oneline option" id="encrypted-class" />
                    </td>
                </tr>
            </tbody>
        </table>
    </fieldset>
</div>

<div class="insert-box">
    <input type="text" name="encrypted" class="tag code" readonly="readonly" onfocus="this.select()" />

    <div class="submitbox">
        <input type="button" class="button button-primary insert-tag" value="<?php echo esc_attr( __( 'Insert Tag', 'contact-form-7' ) ); ?>" />
    </div>
</div>
