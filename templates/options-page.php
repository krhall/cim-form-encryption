<div class="wrap">
    <form action="options.php" method="post">
        <?php

        settings_fields('cim-encrypted-fields-options');
        do_settings_sections('cim-encrypted-fields-options');
        submit_button('Save');

        ?>
    </form>
</div>
