<?php

/**
 * Plugin Name: Compulse Encrypted Fields
 * Description: Adds encrypted fields for the following plugins: Contact Form 7 with Contact Form DB, WP Job Manager - Applications.
 * Author: Compulse, Kevin Hall
 * Author URI: http://compulse.com
 * Version: 1.1.0
 */

// ini_set('display_errors', 1);
// error_reporting(-1);

define('CIM_FORM_ENCRYPTION_PLUGIN_DIR', dirname(__FILE__));

include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/vendor/autoload.php';
include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/includes/classes/CIMContactForm7Encryption.php';
include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/includes/classes/CIMWPJobManagerEncryption.php';
include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/includes/classes/CIMFormEncryptionPlugin.php';

function cim_form_encryption() {
    return CIMFormEncryptionPlugin::get_instance();
}

cim_form_encryption(); // Initialize
