<?php

class CIMWPJobManagerEncryption {
    private static $instance = null;

    public static function get_instance() {
        if ( empty(self::$instance) ) {
            self::$instance = new self();
        }

        return self::$instance;
    }

    private $encrypted_values;

    private $posted_meta;
    private $new_application_post_id;

    private function __construct() {
        $this->encrypted_values = array();
        $this->posted_meta = array();
        $this->new_application_post_id = 0;

        add_filter('job_application_form_field_rules', array($this, 'job_application_form_field_rules'));
        add_filter('job_application_form_posted_meta', array($this, 'job_application_form_posted_meta'), 10, 2);

        add_action('add_meta_boxes', array($this, 'add_resend_meta_box'));
        add_action('wp_ajax_cim_resend_encrypted_job_application', array($this, 'resend_ajax'));
		add_action('wp_ajax_cim_view_encrypted_job_application', array($this, 'view_ajax'));
    }

    /**
     * Hooked on 'job_application_form_field_rules' filter.
     * Adds the encrypted rule.
     */
    public function job_application_form_field_rules($rules) {
        $group_key = __( 'Data Handling', 'wp-job-manager-applications' );

        if ( array_key_exists($group_key, $rules) ) {
            $rules[ $group_key ]['encrypted'] = __( 'Encrypted', 'wp-job-manager-applications' );
        }

        return $rules;
    }

    /**
     * Hooked on 'job_application_form_posted_meta' filter.
     * Triggered when the job application form has been submitted and has passed validation.
     */
    public function job_application_form_posted_meta($meta, $values) {
        // The next application post inserted will be the current application.
        add_action('wp_insert_post', array($this, 'capture_new_application_id'), 10, 3);

        // The next mail sent will be the application notification.
        add_filter('wp_mail', array($this, 'wp_mail'), 50);

        return $meta;
    }

    /**
     * Hooked on 'wp_insert_post' action after job application form is submitted.
     * Captures the new job application ID so it can be used later in the request.
     */
    public function capture_new_application_id($post_id, $post, $update) {
        if ( get_post_type($post_id) == 'job_application' ) {
            $this->new_application_post_id = $post_id;

            remove_action('wp_insert_post', array($this, 'capture_new_application_id'), 10, 3);
        }
    }

    /**
     * Hooked on 'wp_mail' filter after job application form is submitted.
     * This is used for the outgoing email notification.  The message will be encrypted and stored with the application.
     */
    public function wp_mail($args) {
        remove_filter('wp_mail', array($this, 'wp_mail'), 50);

        // Delete any encrypted values.
        $fields = get_job_application_form_fields();
        foreach ( $fields as $field_name => $field_definition ) {
            if ( in_array('encrypted', $field_definition['rules']) ) {
                $this->encrypted_values[ $field_definition['label'] ] = get_post_meta($this->new_application_post_id, $field_definition['label'], true);
                update_post_meta($this->new_application_post_id, $field_definition['label'], '*****');
            }
        }

        if ( !empty( $this->encrypted_values ) ) {
            $encryption_result = cim_form_encryption()->encrypt_message($args['message']);

            if ( !empty($encryption_result) ) {
                $args['message'] = (cim_form_encryption()->get_encryption_method() == 'PGP') ? $encryption_result : 'Job application has been encrypted.  Log in here to view it: ' . admin_url();

                // Save encrypted message to application.
                update_post_meta($this->new_application_post_id, '_encrypted_message', $encryption_result);
            }
        } else {
            // Nothing to encrypt.
        }

        return $args;
    }

    /**
     * Hooked on 'add_meta_boxes' action.
     * Adds the "Resend Encrypted Message" metabox.
     */
    public function add_resend_meta_box() {
		if ( CIMFormEncryptionPlugin::get_instance()->get_encryption_method() == 'PGP' ) {
        	add_meta_box('resend-encrypted-message', 'Resend Encrypted Message', array($this, 'resend_meta_box_content'), 'job_application', 'side', 'low');
		} else {
			add_meta_box('view-encrypted-message', 'View Encrypted Message', array($this, 'view_meta_box_content'), 'job_application', 'side', 'low');
		}
    }

    /**
     * Displays the content for the "Resend Encrypted Message" metabox.
     */
    public function resend_meta_box_content() {
        include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/templates/wp-job-manager-resend-meta-box.php';
    }
	
	public function view_meta_box_content() {
		include CIM_FORM_ENCRYPTION_PLUGIN_DIR . '/templates/wp-job-manager-view-meta-box.php';
	}
	
	public function view_ajax() {
		if ( !current_user_can('administrator') || CIMFormEncryptionPlugin::get_instance()->get_encryption_method() != 'AES' )
            exit;

        $post_filter = filter_input_array(INPUT_POST, [
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ]
        ]);

        //var_dump($post_filter);

        if ( !empty($post_filter['id']) ) {
            $application_id = $post_filter['id'];

            $encrypted_message = get_post_meta($application_id, '_encrypted_message', true);

            if ( !empty($encrypted_message) ) {
				$decrypted = CIMFormEncryptionPlugin::get_instance()->decrypt_aes_message( $encrypted_message );
            	wp_send_json([
					'html' => nl2br($decrypted)
				]);
            } else {
                wp_send_json([
                    'html' => 'There is no encrypted message for this application.'
                ]);
            }
        }

        exit;
	}

    public function resend_ajax() {
        if ( !current_user_can('administrator') )
            exit;

        $post_filter = filter_input_array(INPUT_POST, [
            'to' => FILTER_DEFAULT,
            'id' => [
                'filter' => FILTER_VALIDATE_INT,
                'options' => [
                    'min_range' => 1
                ]
            ]
        ]);

        //var_dump($post_filter);

        if ( !empty($post_filter['to']) && !empty($post_filter['id']) ) {
            $to = $post_filter['to'];
            $application_id = $post_filter['id'];

            $encrypted_message = get_post_meta($application_id, '_encrypted_message', true);

            if ( !empty($encrypted_message) ) {
                wp_mail($to, 'Encrypted Job Application', $encrypted_message);
                wp_send_json_success();
            } else {
                wp_send_json([
                    'error' => 'There is no encrypted message for this application.'
                ]);
            }
        }

        exit;
    }
}
